function Moon(card_number) {
    if (/[^0-9-\s]+/.test(card_number)) {
        return false;
    }

    let nCheck = 0;
    let bEven = false;
    card_number = card_number.replace(/\d/g, "");

    for (let n = card_number.length - 1; n >= 0; n--) {
        let cDigit = card_number.charAt(n);
        let nDigit = parseInt(cDigit, 10);

        if (bEven) {
            if ((nDigit *= 2) > 9) nDigit -= 9;
        }

        nCheck += nDigit;
        bEven = !bEven;
    }

    return (nCheck % 10) == 0;
}

function isActualDate(month, year) {
    let currentDate = new Date();
    let givenDate = new Date(year, month);
    let maxDate = new Date(currentDate.getFullYear() + 4, currentDate.getMonth() + 1);

    return givenDate > currentDate && givenDate <= maxDate;
}

function getErrorMessage() {
    let screenWidth = document.documentElement.clientWidth;

    return screenWidth < 525 ? 'Обязательное поле' : 'Поле обязательно для ввода';
}

function setCardTypeImage(firstNumber) {
    let types = { 3: 'American-express', 4: 'Visa', 5: 'MasterCard' };
    let cardNumber = document.getElementById('card-number');

    if (types.hasOwnProperty(firstNumber)) {
        cardNumber.style.background = 'white url("./src/images/' + types[firstNumber] +'.png") no-repeat 95% center';
        cardNumber.style.backgroundSize = '15%';
    } else {
        cardNumber.style.background = 'white';
    }
}

function setInvalidField(element, message) {
    element.classList.add('invalid');
    element.nextElementSibling.style.visibility = 'visible';
    element.nextElementSibling.innerHTML = '* ' + message;
}

function isValid(event, regexp = /[^0-9 ]/g) {
    let value = event.target.value;

    if (!/\d/g.test(value)) {
        event.target.value = value.replace(regexp, '');
        return;
    }

    event.target.classList.remove('invalid');
    event.target.nextElementSibling.style.visibility = 'hidden';
}

function checkErrors(event, element = '') {
    element = element === '' ? event.target : element;
    let message = getErrorMessage();

    if (element.value.length === 0) {
        setInvalidField(element, message);
        return false;
    } else if (element.value.length !== element.maxLength) {
        setInvalidField(element, 'Некоректные данные');
        return false;
    } else {
        element.classList.remove('invalid');
        element.nextElementSibling.style.visibility = 'hidden';
        return true;
    }
}

function main() {
    let listItems = document.getElementsByTagName('li');
    let cardNumber = document.getElementById('card-number');
    let inspirationDate = document.getElementById('inspiration-date');
    let cvc = document.getElementById('cvc');
    let submit = document.getElementById('submit');
    let loader = document.getElementsByClassName('loader')[0];

    for (let item of listItems) {
        item.addEventListener('click', function () {
            for (let item of listItems) {
                item.classList.remove('selected-language');
            }
            item.classList.add('selected-language');
        })
    }

    cardNumber.addEventListener('input', function (event) {
        isValid(event);
        setCardTypeImage(this.value[0]);

        let cardCode = this.value.replace(/[^\d]/g, '').substring(0,16);
        cardCode = cardCode !== '' ? cardCode.match(/.{1,4}/g).join(' ') : '';
        this.value = cardCode;
    });
    cardNumber.addEventListener('blur', checkErrors);

    inspirationDate.addEventListener('input', function (event) {
        isValid(event);

        let date = this.value.replace(/[^\d]/g, '').substring(0,16);
        date = date !== '' ? date.match(/.{1,2}/g).join('/') : '';
        this.value = date;
    });
    inspirationDate.addEventListener('blur', checkErrors);

    cvc.addEventListener('input', function (event) {
        isValid(event, /[^0-9*]/g);
        this.value = this.value.replace(/./gm, '*');
    });
    cvc.addEventListener('keypress', function (event) {
        if (event.key === '*') {
            cvc.value = cvc.value.substring(0, cvc.value.length - 1);
        }
    });
    cvc.addEventListener('blur', checkErrors);

    submit.addEventListener('click', function (event) {
        event.preventDefault();

        let hasError = [];

        [cardNumber, inspirationDate, cvc].map((element) => {
            hasError.push(checkErrors(null, element));
            console.log(hasError);
        });

        if (hasError.includes(false)) {
            console.log(false);
            return;
        }

        let year = inspirationDate.value.substring(3, 5);
        let month = inspirationDate.value.substring(0, 2);

        loader.style.display = 'inline-block';
        this.setAttribute('disabled', 'true');

        setTimeout(() => {
            loader.style.display = 'none';
        }, 2000);

        setTimeout(() => {
            if (Moon(cardNumber.value) && cvc.value.length === 3 && (month > 0 && month < 12) && isActualDate(month, '20' + year)) {
                alert('Successful');
            } else {
                alert('Wrong!');
            }
        }, 2100);

        this.setAttribute('disabled', 'false');
    });
}

document.addEventListener("DOMContentLoaded", main);